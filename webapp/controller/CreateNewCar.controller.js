/*global location */
sap.ui.define([
	"freelance/com/vn/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"freelance/com/vn/model/formatter",
	"sap/ui/core/routing/History"
], function(BaseController, JSONModel, formatter, History) {
	"use strict";

	return BaseController.extend("freelance.com.vn.controller.CreateNewCar", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function() {
			// this.getRouter().getRoute("createnewcust").attachPatternMatched(this._onObjectMatched, this);
		},

		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================================== */
		_onObjectMatched: function(oEvent) {

		},
		onNavBack: function() {
			this.getRouter().getTargets().display("object");
			// var sPreviousHash = History.getInstance().getPreviousHash(),
			// 	oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			// if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
			// 	history.go(-1);
			// } else {
			// 	oCrossAppNavigator.toExternal({
			// 		target: {
			// 			shellHash: "#Shell-home"
			// 		}
			// 	});
			// }
		}

	});

});