/*global location */
//thuynt
//test merge
sap.ui.define([
	"freelance/com/vn/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"freelance/com/vn/model/formatter",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(BaseController, JSONModel, formatter, History, MessageBox, MessageToast) {
	"use strict";
	var gflag_edit;
	return BaseController.extend("freelance.com.vn.controller.Detail", {
		formatter: formatter,
		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */
		onInit: function() {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0,
				lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading")
			});
			this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
			this.setModel(oViewModel, "detailView");
			this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		},
		/* =========================================================== */
		/* event handlers                                              */
		/* =========================================	================== */
		/**
		 * Event handler when the share by E-Mail button has been clicked
		 * @public
		 */
		onShareEmailPress: function() {
			var oViewModel = this.getModel("detailView");
			sap.m.URLHelper.triggerEmail(null, oViewModel.getProperty("/shareSendEmailSubject"), oViewModel.getProperty(
				"/shareSendEmailMessage"));
		},
		/**
		 * Event handler when the share in JAM button has been clicked
		 * @public
		 */
		onShareInJamPress: function() {
			var oViewModel = this.getModel("detailView"),
				oShareDialog = sap.ui.getCore().createComponent({
					name: "sap.collaboration.components.fiori.sharing.dialog",
					settings: {
						object: {
							id: location.href,
							share: oViewModel.getProperty("/shareOnJamTitle")
						}
					}
				});
			oShareDialog.open();
		},
		/**
		 * Updates the item count within the line item table's header
		 * @param {object} oEvent an event containing the total number of items in the list
		 * @private
		 */
		onListUpdateFinished: function(oEvent) {
			var sTitle, iTotalItems = oEvent.getParameter("total"),
				oViewModel = this.getModel("detailView");
			// only update the counter if the length is final
			if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
				if (iTotalItems) {
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [iTotalItems]);
				} else {
					//Display 'Line Items' instead of 'Line items (0)'
					sTitle = this.getResourceBundle().getText("detailLineItemTableHeading");
				}
				oViewModel.setProperty("/lineItemListTitle", sTitle);
			}
		},
		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */
		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function(oEvent) {
			var sObjectId = oEvent.getParameter("arguments").objectId;
			this.getModel().metadataLoaded().then(function() {
				var sObjectPath = this.getModel().createKey("Customers", {
					CustomerID: sObjectId
				});
				this._bindView("/" + sObjectPath);
			}.bind(this));
		},
		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function(sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("detailView");
			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);
			this.getView().bindElement({
				path: sObjectPath,
				events: {
					change: this._onBindingChange.bind(this),
					dataRequested: function() {
						oViewModel.setProperty("/busy", true);
					},
					dataReceived: function() {
						oViewModel.setProperty("/busy", false);
					}
				}
			});
		},
		_onBindingChange: function() {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();
			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter().getTargets().display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent().oListSelector.clearMasterListSelection();
				return;
			}
			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel().getObject(sPath),
				sObjectId = oObject.CustomerID,
				sObjectName = oObject.CompanyName,
				oViewModel = this.getModel("detailView");
			this.getOwnerComponent().oListSelector.selectAListItem(sPath);
			oViewModel.setProperty("/saveAsTileTitle", oResourceBundle.getText("shareSaveTileAppTitle", [sObjectName]));
			oViewModel.setProperty("/shareOnJamTitle", sObjectName);
			oViewModel.setProperty("/shareSendEmailSubject", oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage", oResourceBundle.getText("shareSendEmailObjectMessage", [
				sObjectName,
				sObjectId,
				location.href
			]));
		},
		_onMetadataLoaded: function() {
			// Store original busy indicator delay for the detail view
			var iOriginalViewBusyDelay = this.getView().getBusyIndicatorDelay(),
				oViewModel = this.getModel("detailView"),
				oLineItemTable = this.byId("lineItemsList"),
				iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();
			// Make sure busy indicator is displayed immediately when
			// detail view is displayed for the first time
			oViewModel.setProperty("/delay", 0);
			oViewModel.setProperty("/lineItemTableDelay", 0);
			oLineItemTable.attachEventOnce("updateFinished", function() {
				// Restore original busy indicator delay for line item table
				oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
			});
			// Binding the view will set it to not busy - so the view is always busy if it is not bound
			oViewModel.setProperty("/busy", true);
			// Restore original busy indicator delay for the detail view
			oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
		},
		onSelectionChange: function(oEvent) {
			var oGrid = this.getView().byId("formExtend");
			var key = this.getView().byId("cb_extend").getSelectedKey();
			switch (key) {
				case "Id":
					// 
					this.getView().byId("grid1").setVisible(true);
					this.getView().byId("grid2").setVisible(false);
					this.getView().byId("grid3").setVisible(false);
					break;
				case "Vin":
					this.getView().byId("grid2").setVisible(true);
					this.getView().byId("grid1").setVisible(false);
					this.getView().byId("grid3").setVisible(false);
					break;
				case "Other":
					this.getView().byId("grid3").setVisible(true);
					this.getView().byId("grid1").setVisible(false);
					this.getView().byId("grid2").setVisible(false);
					break;
				case "No":
					this.getView().byId("grid1").setVisible(false);
					this.getView().byId("grid2").setVisible(false);
					this.getView().byId("grid3").setVisible(false);
					break;
			}
		},
		onUpdate: function(evt) {
			if (evt.getSource().getPressed()) {
				this._fn_ModifyDetailView(true);
				gflag_edit = "X";
			} else {
				this._fn_ModifyDetailView(false);
				gflag_edit = "";
			}
		},
		_fn_ModifyDetailView: function(sMode) {
			var oView = this.getView();
			// Customer editable
			oView.byId("in_custName").setEnabled(sMode);
			oView.byId("in_address").setEnabled(sMode);
			oView.byId("in_mobile").setEnabled(sMode);
			oView.byId("ta_custtex").setEnabled(sMode);
			//Vehicle
			oView.byId("in_cur").setEnabled(sMode);
			oView.byId("in_colour").setEnabled(sMode);
			oView.byId("in_custold").setEnabled(sMode);
			oView.byId("in_custnew").setEnabled(sMode);
			oView.byId("in_salse").setEnabled(sMode);
			oView.byId("in_distri").setEnabled(sMode);
			oView.byId("in_driver").setEnabled(sMode);
			oView.byId("in_reg").setEnabled(sMode);
			oView.byId("in_license_new").setEnabled(sMode);
			oView.byId("ta_vehicle").setEnabled(sMode);
			//Extend
			oView.byId("cb_extend").setEnabled(sMode);
			//Button
			oView.byId("bt_save").setEnabled(sMode);
			oView.byId("bt_cancel").setEnabled(sMode);
		},
		/**
		 *@memberOf freelance.com.vn.controller.Detail
		 */
		onLinkDBMOrderPress: function() {
			// // this._oDialog.setModel(this.getView().getModel());
			// jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			// this.getDialog().open();
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("freelance.com.vn.view.DBMOrderDetailDialog", this);
			}
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
			sap.ui.getCore().byId("vsd_bdmoder-cancelbutton").setVisible(false);
		},
		onButtonOK: function(oEvent) {
			this.getDialog().close();
			this.onSearch(oEvent);
		},
		createNewCust: function(oEvent) {
			var that = this;
			if (gflag_edit === "X") {
				MessageBox.show("Data may be lost.Do you want continue create new Customer?", {
					icon: MessageBox.Icon.WARNING,
					title: "Warning",
					actions: [
						MessageBox.Action.OK,
						MessageBox.Action.CANCEL
					],
					onClose: function(oAction) {
						if (oAction === MessageBox.Action.OK) {
							that.getRouter().getTargets().display("createnewcustomer");
							that.getView().byId("bt_update").setPressed(false);
							that._fn_ModifyDetailView(false);
							that._fn_ResetChange();
							gflag_edit = "";
						} else {
							return;
						}
					}
				});
			} else {
				this.getRouter().getTargets().display("createnewcustomer");
			}
		},
		_fn_ResetChange: function() {
			// Reset change
			var oModel = this.getView().getModel();
			oModel.resetChanges();
		},
		createNewCar: function() {
			var that = this;
			if (gflag_edit === "X") {
				MessageBox.show("Data may be lost.Do you want continue create new Vehicle?", {
					icon: MessageBox.Icon.WARNING,
					title: "Warning",
					actions: [
						MessageBox.Action.OK,
						MessageBox.Action.CANCEL
					],
					onClose: function(oAction) {
						if (oAction === MessageBox.Action.OK) {
							that.getRouter().getTargets().display("createnewcar");
							that.getView().byId("bt_update").setPressed(false);
							that._fn_ModifyDetailView(false);
							that._fn_ResetChange();
							gflag_edit = "";
						} else {
							return;
						}
					}
				});
			} else {
				this.getRouter().getTargets().display("createnewcar");
			}

		},
		/**
		 *@memberOf freelance.com.vn.controller.Detail
		 */
		onSave: function(oEvent) {
			this._fn_SaveUpdate(oEvent);
		},
		/**
		 *@memberOf freelance.com.vn.controller.Detail
		 */
		onCancel: function() {
			var that = this;
			MessageBox.show("Data may be lost.Do you want continue?", {
				icon: MessageBox.Icon.WARNING,
				title: "Warning",
				actions: [
					MessageBox.Action.OK,
					MessageBox.Action.CANCEL
				],
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.OK) {
						that.getView().byId("bt_update").setPressed(false);
						that._fn_ModifyDetailView(false);
						that._fn_ResetChange();
						gflag_edit = "";
					} else {
						return;
					}
				}
			});
		},
		_fn_SaveUpdate: function(oEvent) {
			var oData = oEvent.getParameter("data"),
				oView = this.getView(),
				that = this,
				oModel = this.getModel();
			// abort if the  model has not been changed
			if (oModel.hasPendingChanges()) {
				oModel.attachEventOnce("batchRequestCompleted", function(oEvent) {
					if (that._fn_BatchRequestSucceeded(oEvent)) {
						that.getView().byId("bt_update").setPressed(false);
						that._fn_ModifyDetailView(false);
						gflag_edit = "";
						MessageBox.success("Update Success!");
					} else {
						that.getView().byId("bt_update").setPressed(false);
						that._fn_ModifyDetailView(false);
						that._fn_ResetChange();
						gflag_edit = "";
						MessageBox.error("Update Eror!");
					}
				});
				oModel.submitChanges();
			} else {
				MessageBox.show("Data haven't change.Do you want exit?", {
					icon: MessageBox.Icon.WARNING,
					title: "Warning!",
					actions: [
						MessageBox.Action.OK
					],
					onClose: function(oAction) {
						if (oAction === MessageBox.Action.OK) {
							that.getView().byId("bt_update").setPressed(false);
							that._fn_ModifyDetailView(false);
							gflag_edit = "";
						} else {
							return;
						}
					}
				});
			}
		}
	});
});